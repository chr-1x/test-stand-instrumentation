import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from PySide.QtCore import QTime, QTimer, QDateTime
from pyqtgraph.dockarea import *

import datetime, dateutil, time, sys, bisect, os
from time import sleep
import socket, threading

from os.path import expanduser

# This script will store data files in the user's ~/.vis/ directory. 
# That's typically something like C:/Users/Andrew/.vis/
visPath = expanduser("~/.vis/")
serialLocalPath = expanduser("~/.vis/serial.local.dat")
altFileName = ""
# Make sure the path exists.
if not os.path.exists(visPath):
    os.makedirs(visPath)

### Utility functions that probably exist somewhere in python but are easy enough to express here
def lerp(val, rangemin, rangemax):
    return rangemin + val * (rangemax - rangemin)
def unlerp(val, rangemin, rangemax):
    return (val - rangemin) / (rangemax - rangemin)
def rangemap(val, oldmin, oldmax, newmin, newmax):
    return lerp(unlerp(val, oldmin, oldmax), newmin, newmax)
def findClosestIndex(target, values):
    return bisect.bisect_left(values, target)

def stringToFloatNoException(s):
    ''' Sometimes you just need to get a float, no questions asked. '''
    result = 0
    try:
        result = float(s)
    except ValueError:
        result = float('nan')
    return result

class SensorView():
    ''' Class that groups related data about how each group of sensors should be displayed. '''
    def __init__(self, sensors=[1], colors=['r'], bounds=(0, 5), datamap=((0, 5), (0, 5)), title="sensor", names=["sensor"], raw=False):
        # Note that datamap is given as a pair of tuples. The default interpretation of the datamap variable is as a
        # range the data is given in (e.g. voltage) and a range to map to in a linear fashion (e.g. force). However, if
        # datamap is a callable variable (i.e. a function), it is expected to be a function that takes a data value and
        # returns the mapped value. This is better for mappings that are nonlinear or have values that need to be easily
        # tweaked. 
        self.sensors = sensors
        self.colors = colors
        self.bounds = bounds
        self.datamap = datamap
        self.title = title
        self.names = names
        # Whether or not to show the raw data (pre-mapped, e.g. voltage) on the graph
        self.raw = raw 

        self.valLabels = []
        self.plot = None
        self.curves = []
        self.labels = []
        self.vLine = None
    def map(self, val):
        if callable(self.datamap):
            return self.datamap(val)
        return rangemap(val, self.datamap[0][0], self.datamap[0][1], self.datamap[1][0], self.datamap[1][1])



dm_identity = ((0, 1), (0, 1))
def loadCellMap(val):
    VRef = 5.0
    ZeroOffset = 4.164
    PoundsPerVolt = -45.1745
    return (val - ZeroOffset) * PoundsPerVolt
def pressureMap(val):
    vRange = 3000
    psiRange = 1000
    return (val / vRange) * psiRange

### This is where the layout of the visualization is defined! Explanation of the parameters:
#    sensors: A list of the indices into the sensor data array to display, effectively which "sensor numbers" to put on this row. 
#             Make sure these match the order they are output from the Arduino and serial read program.
#    colors:  A list of Qt-compatible color definitions to display the sensor data in. This should be the same length as sensors.
#    bounds:  The axis bounds for this group of sensors. In whatever units the value is being displayed in.
#    datamap: DO NOT USE! Leave as dm_identity. All range mapping should be done in the serial program.
#    title:   What to show above the graph.
#    names:   A list of strings that will be the legend names for each line
#    raw:     A boolean value indicating whether or not to show raw (i.e. voltage) values on the graph.
views = [ SensorView(sensors=[1], colors=[0.3], bounds=(-150, 150), datamap=loadCellMap, title="Load (lbs)", names=["load"], raw=False),
         SensorView([2], ['b'], (0, 400), pressureMap, "Pressure (psi)", names=["pressure"], raw=False),
         SensorView([3, 4, 5, 6], ['r', (50, 200, 0), (0, 0, 255), (220, 150, 0)], (-270, 1370), dm_identity, "Temperature (C)", names=["T3", "T4", "T5", "T6"], raw=True) ] 

def parseSensorLine(line): 
    ''' Function that processes a line of text sent over from the serial program. Reads all the numbers as floats and the timestamp 
        as its particular format. If the time is not able to be parsed, it is replaced with None, and later parts of the
        code that deal with these should expect to see Nones where they would normally see a number of milliseconds.'''
    orig = line.split(',') 
    timestamp = orig[0]
    orig = orig[1:]

    fmt = "yyyy-MM-dd HH:mm:ss.zzz"
    time = QDateTime.fromString(timestamp[1:], fmt)
    if (not time.isValid()):
        time = None 
    else: time = time.toMSecsSinceEpoch()

    vals = [stringToFloatNoException(i) for i in orig] 
    vals = [time] + vals

    return vals

class DataReceiver():
    ''' Class that exists because Python globals aren't good with threads. Wraps the functionality of listening/yielding data
        from the sensor program. '''
    COUNT = 1000

    # Hostname and port are e.g. 192.168.1.187 8888
    def __init__(self, hostname, port):
        ### NOTE: sensorData must contain AT LEAST AS MANY initial values as you intend to receive from serial.
        self.sensorData =   [ [0],[0],[0],[0],[0],[0],[0],[0],[0],[0]] 
        self.historicData = [ [0],[0],[0],[0],[0],[0],[0],[0],[0],[0]] 
        self.tail = ""
        self.hostname = hostname
        self.port = port
        self.first = True
        self.fileBuffer = ""
        self.reset = False

    def readSensorFile(self, startT, endT):
        ''' Yields all sensor readings in the locally stored file from startT to endT. This can be the empty set. '''
        if (startT is not None and endT is not None):
            print("Checking sensor file between %s and %s...\n" % (startT.toString("yyyy-MM-dd HH:mm:ss.zzz"), endT.toString("yyyy-MM-dd HH:mm:ss.zzz")))
        else:
            print("Checking data file %s...\n" % altFileName)
        # Assumption 1: startT and endT are QDateTimes 
        # Assumption 2: "serial.local.dat" exists
        # Assumption 3: serial.local.dat has strictly ordered timestamps
            # Note: Assumption 3 can be broken if the time changes in whatever you are running the serial program
            # on. Make sure it is consistent between datasets.
        result = [ [0],[0],[0],[0],[0],[0],[0],[0],[0],[0],[0],]
        l = 0
        if (altFileName != ""): fname = altFileName
        else: fname = serialLocalPath
        with open(fname, "r") as data:
            for line in data:
                vals = parseSensorLine(line)
                time = vals[0]
                if (time is not None):
                    # Timestamp is valid
                    qtime = QDateTime.fromMSecsSinceEpoch(time)
                    if ((startT is None and endT is None) or (qtime >= startT and qtime <= endT)):
                        for i,val in enumerate(vals):
                            if l == 0:
                                # We don't want the initial zero value from above. We could just as easily use an
                                # append, but this makes it explicit how many values are expected and where each will
                                # end up.
                                result[i] = [val]
                            else:
                                result[i].append(val)
                        l += 1
        self.historicData = result
        print("Done tabulating historic data (%d entries)\n" % len(result[0]))


    # Theoretically sockets have a certain buffer size that they like but I'm not sure 
    # that's true in practice on most modern systems, at least with TCP-IPv4.
    def GetMessageMaxSize(Socket):
        return Socket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF)

    def receiveSensorData(self):
        while (True):
            print("Connecting to %s:%s\n" % (self.hostname, self.port))
            try:
                Client = socket.create_connection((self.hostname, self.port), timeout=10)
                self.reset = False

                MessageSize = DataReceiver.GetMessageMaxSize(Client)
                if (MessageSize <= 0):  MessageSize = 1024

                Result = 1
                while (Result > 0 and not self.reset):
                    Buffer = Client.recv(MessageSize)

                    uBuffer = Buffer.decode("utf-8")
                    self.fileBuffer += uBuffer

                    if (len(self.fileBuffer) > 2048):
                        with open(serialLocalPath, "a") as data:
                            data.write(self.fileBuffer)
                            self.fileBuffer = ""

                    Result = len(Buffer)
                    if (Result > 0):
                        # self.tail is the portion of the buffer that was not a full line last time.
                        BufferStr = self.tail + uBuffer

                        Lines = BufferStr.split("\n")

                        for Line in Lines[:-1]:
                            parsed = parseSensorLine(Line)
                            if (parsed[0] is not None):
                                # Timestamp is valid, we're good
                                for i, val in enumerate(parsed):
                                    if (self.first):
                                        self.sensorData[i] = [val]
                                    elif (len(self.sensorData[0]) < DataReceiver.COUNT):
                                        self.sensorData[i].append(val)
                                    else:
                                        # Maintain a count of COUNT 
                                        self.sensorData[i] = self.sensorData[i][1:] + [val]
                                if (self.first):
                                    self.first = False
                        self.tail = Lines[-1]

                # Shutting down is important. It lets the serial program know to free up the port.
                Client.shutdown(socket.SHUT_WR)
                Client.close()
                print("Lost connection, attempting to reacquire...")

            except ConnectionRefusedError:
                print("Connection refused. Trying again in 2 seconds.")
                sleep(2)

            except Exception as e:
                print("Unknown exception (probably timeout). Trying again in 2 seconds. (Message: %s, ex: %s)" % (e.strerror, str(e)))
                sleep(2)

    def getRecentValues(self, ind, n):
        ''' Function that returns the first n cached recent values straight from the serial program. In order to
            populate this array, receiveSensorData must be looping on another thread. '''
        return self.sensorData[ind][-n:-1]

    def getHistoricValues(self, ind, ):
        ''' Function that returns all of the currently cached historic values. In order to populate this array, you need
            to call readSensorFile with a start time and end time. If you want to read a specific file, the global
            variable altFileName must be set to the name of the file.'''
        return self.historicData[ind]


class TimeAxisItem(pg.AxisItem):
    ''' Class that adds time-axis functionality to pyqtgraph graphing in the dumbest possible way.'''
    def tickStrings(self, values, scale, spacing):
        return [QDateTime.fromMSecsSinceEpoch(value).toString('HH:mm:ss') for value in values]

class FixedLegendItem(pg.LegendItem):
    ''' Adds an updateItem function to LegendItem so that I can actually update the 
        item text without adding/removing items every frame.'''
    def updateItem(self, name, newname):
        for sample, label in self.items:
            if label.text == name:
                label.setText(newname)
            #self.updateSize()
 

### Begin Program Initialization
app = QtGui.QApplication([])
win = QtGui.QMainWindow()
area = DockArea()
win.setCentralWidget(area)

pg.setConfigOptions(antialias=True)
# Use white-on-black color scheme for readability
pg.setConfigOption('background', 'w')
pg.setConfigOption('foreground', 'k')

win.resize(1000,600)
win.setWindowTitle('Engine Test Visualization')
win.show()

# Add graph GUI
for v in views:
    # Plot title. Use a large font.
    v.dock = Dock(v.title, size=(800, 200))
    area.addDock(v.dock, 'top')
    v.plot = pg.PlotWidget(title='<span style="font-size:20pt;padding:5pt;">%s</span>'%v.title, axisItems={'bottom': TimeAxisItem(orientation='bottom')})
    v.dock.addWidget(v.plot)

    # Enable grid and x-axis autorange, and set the y-axis range to the configured value
    v.plot.showGrid(x=True, y=True)
    v.plot.getViewBox().setXRange(0, 20000) # In milliseconds
    v.plot.getViewBox().setYRange(v.bounds[0], v.bounds[1])

    v.plot.setDownsampling(mode='peak')
    v.plot.setClipToView(True)

    # Add the legend, which will also show the min/max values.
    # Use a FixedLegendItem instead of a pg.LegendItem because we'll be updating it frequently.
    v.legend = FixedLegendItem(size=(250, 30 * len(v.sensors)), offset=(-60,60))
    v.legend.setParentItem(v.plot.getPlotItem())

    # Make a plot for each sensor in our defined views. Set the colors and names.
    for i in range(len(v.sensors)):
        v.curves.append(v.plot.plot(pen=pg.mkPen(v.colors[i], width=3), name=v.names[i]))
        v.legend.addItem(v.curves[i], v.names[i])
        v.labels.append(v.names[i])

    # Add the crosshair line
    v.vLine = pg.InfiniteLine(angle=90, movable=False)
    v.plot.addItem(v.vLine, ignoreBounds=True)

#Add misc GUI
guiDock = Dock("Options/Settings", size=(200, 600))
area.addDock(guiDock, 'right')
widg = QtGui.QWidget()
widgl = QtGui.QVBoxLayout()
widg.setLayout(widgl)
guiDock.addWidget(widg)

# Server settings
# Get the relevant data for connecting to the server
# You can call the script with arguments to avoid a pop-up box, e.g.:
#  python vis.py 192.168.1.187
hostname = "localhost"
if (len(sys.argv) >= 2):
    # Check first in the command line arguments.
    hostname = sys.argv[1]
else:
    hostInp,ok = QtGui.QInputDialog.getText(None, "Enter host IP address", "Host IP", QtGui.QLineEdit.Normal, "localhost") 
    if hostInp:
        hostname = hostInp
port = "8888"
if (len(sys.argv) >= 3):
    port = sys.argv[2]


# Server settings panel
# Editing either of the boxes here should let you click the button again
def editServerInfo():
    global connectBtn
    connectBtn.setCheckable(True)

# Clicking the button should update the information used by dataSrc
def connectBtnPress():
    global serverIP, sPort, connectBtn, dataSrc
    dataSrc.hostname = serverIP.text()
    dataSrc.port = sPort.text()
    dataSrc.reset = True
    connectBtn.setCheckable(False)

serverIP = QtGui.QLineEdit(hostname)
serverIP.textChanged.connect(editServerInfo)
serverIPLabel = QtGui.QLabel("Host:")
serverIPLabel.setBuddy(serverIP)

sPort = QtGui.QLineEdit(port)
sPort.textChanged.connect(editServerInfo)
sPortLabel = QtGui.QLabel("Port:")
sPortLabel.setBuddy(sPort)

connectBtn = QtGui.QPushButton("Connect")
connectBtn.setCheckable(False)
connectBtn.clicked.connect(connectBtnPress)

serverGroup = QtGui.QGroupBox("Server settings")
serverGroupLayout = QtGui.QGridLayout()
serverGroupLayout.addWidget(serverIPLabel, row=0,col=0)
serverGroupLayout.addWidget(serverIP, row=0,col=1)
serverGroupLayout.addWidget(sPortLabel, row=1,col=0)
serverGroupLayout.addWidget(sPort, row=1,col=1)
serverGroupLayout.addWidget(connectBtn, row=2, col=0, colspan=2)
serverGroup.setLayout(serverGroupLayout)
widgl.addWidget(serverGroup, row=0, col=0)

# Whether to show current or past data
showCurrent = True
ghostCurrent = False
stale = False
def currentBtnTrigger():
    global tStartEdit, tEndEdit, showCurrent, views, altFileName, ghostCurrent
    tStartEdit.setReadOnly(True)
    tStartEdit.setFrame(False)
    tEndEdit.setReadOnly(True)
    tEndEdit.setFrame(False)
    # These buttons manipulate a set of global variables that control what is done when the plot updates. showCurrent
    # controls where the data is sourced from, and altFileName controls which file historic data is sourced from.
    # ghostCurrent is a usability feature and makes the graph show the current data until it goes out of range.
    altFileName = ""
    showCurrent = True
    ghostCurrent = False
    for v in views:
        v.plot.getViewBox().setXRange(0, 20000) # In milliseconds

def oldBtnTrigger():
    global tStartEdit, tEndEdit, showCurrent, ghostCurrent
    tStartEdit.setReadOnly(False)
    tStartEdit.setFrame(True)
    tEndEdit.setReadOnly(False)
    tEndEdit.setFrame(True)
    showCurrent = False
    ghostCurrent = True

currentBtn = QtGui.QRadioButton("Current data")
currentBtn.setChecked(True)
currentBtn.clicked.connect(currentBtnTrigger)
oldBtn = QtGui.QRadioButton("Earlier data")
oldBtn.clicked.connect(oldBtnTrigger)

btnGroup = QtGui.QGroupBox("Data to show")
btnGroupLayout = QtGui.QGridLayout()
btnGroupLayout.addWidget(currentBtn,row=0,col=0)
btnGroupLayout.addWidget(oldBtn,row=0,col=1)
btnGroup.setLayout(btnGroupLayout)

# Timeline edit panel
def timeEdit():
    global tLookupBtn
    # Note: setCheckable() does not seem to actually control whether or not you can click the button. 
    # I'm not sure what function does.
    tLookupBtn.setCheckable(True)

def timeLookup():
    global stale, tStartEdit, tEndEdit, tLookupBtn, ghostCurrent
    # Start a new thread to load the historic data in the range specified by the user, then wait until it completes to
    # set the stale variable.
    ghostCurrent = False
    tLookupBtn.setCheckable(False)
    t = threading.Thread(target=dataSrc.readSensorFile, args=(tStartEdit.dateTime(), tEndEdit.dateTime()))
    t.daemon = True
    t.start()
    t.join()
    stale = True

def timeLoadFile():
    global altFileName, views, ghostCurrent, showCurrent, stale, dataSrc, visPath
    altFileName,ok = QtGui.QFileDialog.getOpenFileName(None, "Open data file", visPath, "Data file (*.dat)")
    if (ok):
        # Got a data file. Make sure the historic data matrix reflects this.
        showCurrent = False
        ghostCurrent = False
        t = threading.Thread(target=dataSrc.readSensorFile, args=(None, None))
        t.daemon = True
        t.start()
        t.join()
        x = dataSrc.getHistoricValues(0)
        for v in views:
            v.plot.getViewBox().setXRange(x[1], x[-2]) 
        tStartEdit.setDateTime(QDateTime.fromMSecsSinceEpoch(x[1]))
        tEndEdit.setDateTime(QDateTime.fromMSecsSinceEpoch(x[-2]))
    else:
        # No valid file from the dialog. Set the global variables so they show the current data.
        showCurrent = True
        ghostCurrent = False
        stale = False
        altFileName = ""

tStartEdit = QtGui.QDateTimeEdit()
tStartEditLabel = QtGui.QLabel("Start:")
tStartEditLabel.setBuddy(tStartEdit)
tStartEdit.setFrame(False)
tStartEdit.setDateTime(QDateTime.currentDateTime().addSecs(-20))
tStartEdit.dateTimeChanged.connect(timeEdit)
tEndEdit = QtGui.QDateTimeEdit()
tEndEditLabel = QtGui.QLabel("End:")
tEndEditLabel.setBuddy(tEndEdit)
tEndEdit.setFrame(False)
tEndEdit.setDateTime(QDateTime.currentDateTime())
tEndEdit.dateTimeChanged.connect(timeEdit)

tLookupBtn = QtGui.QPushButton("Load data")
tLookupBtn.clicked.connect(timeLookup)
tFileBtn = QtGui.QPushButton("Load from...")
tFileBtn.clicked.connect(timeLoadFile)

tlGroup = QtGui.QGroupBox("Timeline settings")
tlGroupLayout = QtGui.QVBoxLayout()
tlGroupLayout.addWidget(tStartEditLabel)
tlGroupLayout.addWidget(tStartEdit)
tlGroupLayout.addWidget(tEndEditLabel)
tlGroupLayout.addWidget(tEndEdit)

tlGroupLayout.addWidget(tLookupBtn)
tlGroupLayout.addWidget(tFileBtn)
tlGroup.setLayout(tlGroupLayout)
tlGroup.setChecked(False)
btnGroupLayout.addWidget(tlGroup, row=1, col=0, colspan=2)
widgl.addWidget(btnGroup, row=1, col=0)
widgl.insertStretch(-1)
# The stretch makes it not take up all the possible space, but pack nicely near the top.

# Crosshair readout
crLabel = QtGui.QLabel("X:0, Y:0")

# Hostname, port acquired above
dataSrc = DataReceiver(hostname, port)

def update():
    ''' Function that updates the graph at regular intervals. '''
    global dataSrc, views, tStartEdit, tEndEdit, showCurrent, stale, ghostCurrent

    if showCurrent or ghostCurrent:
        x = dataSrc.getRecentValues(0, DataReceiver.COUNT)
        if (len(x) > 1 and not ghostCurrent):
            tStartEdit.setDateTime(QDateTime.fromMSecsSinceEpoch(x[0]))
            tEndEdit.setDateTime(QDateTime.fromMSecsSinceEpoch(x[-1]))
    else:
        x = dataSrc.getHistoricValues(0)

    if True: # There used to be a condition here, but python makes it annoying to change things like that.

        # NOTE: Because the data source is on another thread, it can add items between 
        # when we request the X values and when we request the Y values, for example. 
        # So we sidestep this by only requesting as many Y values as we got X values.
        # This is only a problem as long as there are fewer than DataReceiver.COUNT values in the table.

        # NOTE 2: Added the + 1 because otherwise it returns 1 too few. I suspect it's actually 
        # a problem with getRecentValues
        realn = len(x) + 1

        for v in views:
            ((xmin, xmax), (ymin, ymax)) = v.plot.getViewBox().viewRange()
            if realn > 1 and showCurrent:
                v.plot.getViewBox().setXRange(x[-1] - (xmax - xmin), x[-1], padding=0)
            elif stale:
                v.plot.getViewBox().setXRange(tStartEdit.dateTime().toMSecsSinceEpoch(), tEndEdit.dateTime().toMSecsSinceEpoch())

            for i,curve in enumerate(v.curves):
                if showCurrent or ghostCurrent:
                    yu = dataSrc.getRecentValues(v.sensors[i], realn)
                    y = [v.map(i) for i in yu]
                else:
                    yu = dataSrc.getHistoricValues(v.sensors[i])
                    y = [v.map(i) for i in yu]

                try:
                    curve.setData(x, y)
                except Exception:
                    pass
                    # Sometimes there would be problems where the lengths of x and y were different and exceptions would
                    # be thrown. This no longer seems to be the case after realn was used to control it.
                    #print("DEBUG (EX)\n x(%d): %s\ny(%d): %s\n" % (len(x), str(x), len(y), str(y))) 

                if realn > 1:
                    # Update the crosshair and value labels
                    mPos = QtGui.QCursor.pos()
                    mPos = v.plot.mapFromGlobal(mPos)
                    mPos = v.plot.getViewBox().mapSceneToView(mPos)
                    
                    v.vLine.setPos(mPos.x())
                    index = findClosestIndex(mPos.x(), x)
                    if (index < 0): index = -1
                    if (index > len(y) - 1): index = len(y) - 1
                    if (v.raw): labelStr = '<span style="font-size:17pt;">%s: %.3f min %.3f max %.3f</span>' % (v.names[i], y[index], min(y), max(y)) 
                    else: labelStr = '<span style="font-size:17pt;">%s: %.3f min %.3f max %.3f</span><br><span style="font-size:16pt;">raw: %.3f min %.3f max %.3f</span>' % (v.names[i], y[index], min(y), max(y), yu[index], min(yu), max(yu)) 
                    v.legend.updateItem(v.labels[i], labelStr) 
                    v.labels[i] = labelStr
        if stale: stale = False

### This is the thread that controls data received from the serial program.
t = threading.Thread(target=dataSrc.receiveSensorData)
t.daemon = True
t.start()

### Animation timer. Tries to hit 20fps. In reality the perceived smoothness is mostly dependent on how quickly it can
# get data from the serial program. 
timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(50)

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
