#define MS_PER_SEC(S) ((S) * 1000)
#define MS_PER_MIN(M) (MS_PER_SEC(M) * 60)
#define MS_PER_HOUR(H) (MS_PER_MIN(H) * 60)

#define S_PER_MIN(M) ((M) * 60)
#define S_PER_HOUR(H) (S_PER_MIN(H) * 60)
#define S_PER_DAY(D) (S_PER_HOUR(D) * 24)

inline uint32
SafeTruncateUInt64(uint64 Value)
{
    //TODO(chronister): Defines for max values
    Assert(Value <= UINT32_MAX);
    return((uint32)Value);
}

/* =====================
      Math Operations 
   ===================== */

//TODO(chronister): Generalize these to all number types!

//TODO(chronister): Overflow?
//TODO(chronister): Intrinsics?
int32
Pow(int32 Value, int32 Power)
{
    int32 Result = 1;
    while(Power-- > 0)
    {
        Result *= Value;
    }
    return Result;
}

#define Log10(Value) LogN(Value, 10)

int32
LogN(int32 Value, int32 Base)
{
    if (Value < Base) { return 0; }
    int32 Result = 0;
    while (Value > Base - 1)
    {
        Value /= Base;
        Result++;
    }
    return Result;
}

