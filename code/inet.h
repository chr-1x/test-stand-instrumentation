// Theoretically, this should actually be platform agnostic with the right
// #defines...
#if !defined(SOCKET_H)

#if !defined(INVALID_SOCKET)
	#define INVALID_SOCKET -1
#endif

#if !defined(SOCKET_ERROR)
	#define SOCKET_ERROR -1
#endif

#if !defined(SD_SEND)
	#if defined(SHUT_WR)
		#define SD_SEND SHUT_WR
	#else
		#define SD_SEND 1
	#endif
#endif

#if !defined(MSG_NOSIGNAL)
	// It's probably not supported, so just define it to a null flag.
	#define MSG_NOSIGNAL 0
#endif

struct connection
{
	SOCKET Socket;
};

struct net_data
{
	//TODO(chronister): We don't keep track of the capacity vs used size right now, should that be a thing (like strings)?
	uint8* Content;
	uint32 Size;
};

//NOTE: The following functions need to be defined by platform layers
int
GetSocketError();

internal bool
InitializeSockets();

internal void 
CleanupSockets();
// End function signatures

internal uint32 
GetMessageMaxSize(connection Connection)
{
	socklen_t OptLen;
	uint8 OptVals[8];
	int ErrorCode = getsockopt(Connection.Socket, SOL_SOCKET, SO_RCVBUF, (char*)OptVals, &OptLen);
	if (ErrorCode == 0)
	{
		if (OptLen == 4) { 
			uint32* OptVal = (uint32*)(OptVals);
			return *OptVal;
		}
		else if (OptLen == 8) {
			uint64* OptVal = (uint64*)(OptVals);
			return (uint32)(*OptVal);
		}
		else
		{
			LogF(plat::LOG_DEBUG, "Unknown option value from getsockopt for max message size (%d)\n", OptLen);
		}
	}
	else 
	{
		LogF(plat::LOG_DEBUG, "Couldn't get max message size (WSA error %d).\n", GetSocketError);
	}
	return 0;
}

internal bool
SendPart(connection Connection, net_data DataBuffer, uint32 BytesToSend)
{
	BytesToSend = Min(BytesToSend, DataBuffer.Size);
	int iResult = send( Connection.Socket, (char*)DataBuffer.Content, BytesToSend, MSG_NOSIGNAL);

	if (iResult == SOCKET_ERROR) {
	 	LogF(plat::LOG_WARNING, "Send failed with error: %d\n", GetSocketError);
		return false;
	}
	return true;
}

internal bool
Send(connection Connection, net_data DataBuffer)
{
	return SendPart(Connection, DataBuffer, DataBuffer.Size);
}

internal bool 
Send(connection Connection, char* String)
{
	net_data Buffer;
	Buffer.Content = (uint8*)String;
	Buffer.Size = StringLength(String);
	return Send(Connection, Buffer);
}

internal int
Receive(connection Connection, char* Result, int ResultLen)
{
	return recv(Connection.Socket, Result, ResultLen, 0);	
}

internal int
Receive(connection Connection, net_data* Result)
{
	return Receive(Connection, (char*)Result->Content, Result->Size);	
}

internal connection 
ConnectOverTCP(char* Hostname, char* Port)
{
	connection Result = { INVALID_SOCKET };
	
	// Resolve the server address and port
	addrinfo Hints = {};
	Hints.ai_family = AF_UNSPEC;
	Hints.ai_socktype = SOCK_STREAM;
	Hints.ai_protocol = IPPROTO_TCP;

	addrinfo* AddressResult = null;

	int iResult = getaddrinfo(Hostname, Port, &Hints, &AddressResult);
	if (iResult != 0) 
	{
		return Result;
	}

	// Attempt to connect to an address until one succeeds
	for(addrinfo* ptr=AddressResult; 
		ptr != null; 
		ptr=ptr->ai_next) 
	{

		// Create a connection for connecting to server
		Result.Socket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (Result.Socket == INVALID_SOCKET) {
			return Result; 
		}

		// Connect to server.
		iResult = connect( Result.Socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(Result.Socket);
			Result.Socket = INVALID_SOCKET;
			continue;
		}
		sockaddr_in* IPV4 = (sockaddr_in*)ptr->ai_addr;
		char* AddrStr = inet_ntoa(IPV4->sin_addr);
		LogF(plat::LOG_INFO, "Connecting to %s at %s/%u\n", 
			Hostname, 
			AddrStr,
			(IPV4->sin_port >> 8) + ((IPV4->sin_port & 0xFF) << 8));
		break;
	}

	freeaddrinfo(AddressResult);

	return Result;
}

internal connection 
ListenOverTCP(char* Port)
{
	connection Result = { INVALID_SOCKET }; 

	addrinfo AddressHints;
	ZeroMemory(&AddressHints, sizeof(addrinfo));
	AddressHints.ai_family = AF_INET;
	AddressHints.ai_socktype = SOCK_STREAM;
	AddressHints.ai_protocol = IPPROTO_TCP;
	AddressHints.ai_flags = AI_PASSIVE;

	int ErrorCode = 0;

	// Resolve the server address and port
	addrinfo* AddressResult; 
	ErrorCode = getaddrinfo(NULL, Port, &AddressHints, &AddressResult);
	if (ErrorCode != 0 ) {
		LogF(plat::LOG_WARNING, "Couldn't resolve server address (WSA error %d).\n", ErrorCode);
		return Result;
	}

	// Create a SOCKET for connecting to server
	SOCKET ListenSocket = socket(AddressResult->ai_family, AddressResult->ai_socktype, AddressResult->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		freeaddrinfo(AddressResult);
		return Result;
	}

	// Setup the TCP listening socket
	ErrorCode = bind( ListenSocket, AddressResult->ai_addr, (int)AddressResult->ai_addrlen);
	if (ErrorCode == SOCKET_ERROR) {
		LogF(plat::LOG_WARNING, "Couldn't bind to port (WSA error %d)\n", GetSocketError());
		freeaddrinfo(AddressResult);
		closesocket(ListenSocket);
		return Result;
	}

	sockaddr_in* IPV4 = (sockaddr_in*)AddressResult->ai_addr;
	LogF(plat::LOG_INFO, "Listening on port %d\n", ntohs(IPV4->sin_port)); 
	freeaddrinfo(AddressResult);

	ErrorCode = listen(ListenSocket, SOMAXCONN);
	if (ErrorCode == SOCKET_ERROR) {
		LogF(plat::LOG_WARNING, "Unable to listen (WSA error %d)\n", GetSocketError());
		closesocket(ListenSocket);
		return Result;
	}

	Result.Socket = ListenSocket;
	return Result;
}

internal connection 
AcceptClient(connection ListenSocket)
{
	connection Result = { INVALID_SOCKET };
	// Accept a client socket
	Result.Socket = accept(ListenSocket.Socket, NULL, NULL);
	if (Result.Socket == INVALID_SOCKET) {
		LogF(plat::LOG_WARNING, "Couldn't accept connection (WSA error %d)\n", GetSocketError());
	}

	return Result; 
}

internal void 
CloseConnection(connection Connection)
{
	closesocket(Connection.Socket);
}


bool DisableSend(connection Connection)
{
	int iResult = shutdown(Connection.Socket, SD_SEND);
	Connection.Socket = INVALID_SOCKET;
	return iResult != SOCKET_ERROR;
}

#define SOCKET_H
#endif
