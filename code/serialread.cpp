#include "inet.h"

#include "../arduino/channels.h"

// This file is where the real business happens.
// For things not defined here, look in:
//    code/windows_serialread.cpp for windows-specific functions to do with this project
//    code/linux_serialread.cpp for linux-specific functions to do with this project
//    code/chr_ headers for various utility functions. 
//    Anything prefixed with plat:: will be in chr_platform.h, chr_winplatform.h, or chr_linuxplatform.h


// Data structure used to store the converted values from the sensors.
struct sensor_values
{
	plat::time Timestamp;
	// Use a union so we can address them individually or monotonically, depending on the situation.
	union
	{
		struct
		{
			float LoadValues[LOAD_CHANNELS];
			float PressureValues[PRES_CHANNELS];
			float ThermocoupleValues[TEMP_CHANNELS];
		};
		float Values[CHANNELS];
	};
};

sensor_values
ParseSerialValues(char* Line, plat::time Time)
{
	sensor_values Result = {};	
	Result.Timestamp = Time;

	//This assert is for DEBUG ONLY. In the real deal we want to just
	//ignore malformed lines. 
	//Assert(StringOccurrences(STR(Line), STR(",")) == CHANNELS);

	int32 Cursor = 0, NumStart = -1, NumbersFound = 0;
	for (Cursor; Cursor < (int32)StringLength(Line); ++Cursor)
	{
		if (IsNumber(Line[Cursor])
		&& (Cursor == 0 || !IsNumber(Line[Cursor - 1])))
		{
			NumStart = Cursor;
		}

		else if (Line[Cursor] == ','
			&& IsNumber(Line[Cursor - 1])
			&& NumStart >= 0)
		{
			// NOTE!! IT IS ESSENTIAL that the arduino provides only integers here, unless you use another function that
			// can convert floating point such as the standard library atof. 
			parse_int_result Parse = ParseInteger(Cursor-NumStart, Line + NumStart);

			if (NumbersFound < LOAD_CHANNELS)
			{
				if (Parse.Valid && Parse.Value >= 0 ) 
				{ 
					float VRef = 5.0f;

					/*float ZeroOffset = 2.65f;
					float VoltsPerGram = -4.883e-5f; // From first calibration
					float GramsPerPound = 453.592f;
					Result.Values[NumbersFound] = (((Parse.Value / 1024.f) * VRef) - ZeroOffset) / (VoltsPerGram* GramsPerPound); */

					// We ended up preferring to the do the conversion on the python end rather than on the C++ end for
					// ease of looking at raw values and tweaking scale.
					Result.Values[NumbersFound] = (Parse.Value / 1024.f) * VRef; // In Volts.
				}
				else
				{
					Result.Values[NumbersFound] = NAN;
				}
			}
			else if (NumbersFound < LOAD_CHANNELS + PRES_CHANNELS)
			{
				if (Parse.Valid)
				{
					float ADCMultiplier = 0.1875f; 
					/*float PressureRangePSI = 1000.f;
					float ADCMaxMilliVolts = 100.f;
					Result.Values[NumbersFound] = Parse.Value * ADCMultiplier * (PressureRangePSI / ADCMaxMilliVolts); */
					Result.Values[NumbersFound] = Parse.Value * ADCMultiplier; // In milliVolts
				}
				else
				{
					Result.Values[NumbersFound] = NAN;
				}
			}
			else if (NumbersFound < LOAD_CHANNELS + TEMP_CHANNELS)
			{
				if (Parse.Valid)
				{
					// We actually get it as degrees C right off the chip, just
					// need to convert to float 
					float Multiplier = 0.25f;
					Result.Values[NumbersFound] = Parse.Value * Multiplier;
				}
				else
				{
					Result.Values[NumbersFound] = NAN;
				}
			}
			++NumbersFound;
		}
	}

	return Result;
}

string
SerializeSensorValues(sensor_values Values)
{
	string Result = AllocateString(256);
	plat::FormatTimeInto(&Result, "%y-%m-%d %H:%M:%s.%S,", plat::GetTimeLocal());

	int i;
	for (i = 0; i < LOAD_CHANNELS; ++i)
	{
		AppendFullFormatIntoString(&Result, "%.3f,", Values.LoadValues[i]);
	}
	for (i = 0; i < PRES_CHANNELS; ++i)
	{
		AppendFullFormatIntoString(&Result, "%.0f,", Values.PressureValues[i]);
	}
	for (i = 0; i < TEMP_CHANNELS; ++i)
	{
		AppendFullFormatIntoString(&Result, "%.2f,", Values.ThermocoupleValues[i]);
	}
	AppendToString(&Result, STR("\n"));

	return Result;
}

#define PORT "8888"

// This function writes each data line to the backup file (serial.dat) locally as well as adding it to the queue of
// values to send to clients.
// BIG FAT NOTE: Anything to do with threading and using this program as a server was ONLY IMPLEMENTED ON LINUX! This
// code will _NOT_ work on windows unless you build in threading and a server socket in the windows platform layer!
void
YieldLine(string Data, HANDLE DataFile, data_queue* DataQueue, bool32 SaveToFile = true)
{
	if (SaveToFile) { AppendToFile(DataFile, Data); }
	LogF(plat::LOG_DEBUG, "%s", Data.Value);

	if (DataQueue != null)
	{
		DataQueue->LastWrittenItem = (DataQueue->LastWrittenItem + 1) % QUEUE_SIZE;

		data_queue_item* WriteItem = &DataQueue->Items[DataQueue->LastWrittenItem];
		WriteItem->Data = Data;
		WriteItem->Valid = true;

	}
}

// The main loop of the program, which reads from the serial line (arduino), parses, and then yields to the function above.
// It takes two HANDLEs, which are file handles on windows and an alias for integer file descriptors on linux. 
void
SerialReadWriteLoop(HANDLE SerialHandle, HANDLE DataFile, data_queue* DataQueue)
{
	char TextBuffer[256] = {};
	uint32 TextCursor = 0;
	bool32 FirstRun = true;
	while (true){

		char Buffer = 0;
		// We read from the serial file 1 character at a time.
		if (!ReadFromFile(SerialHandle, &Buffer, 1))
		{
			LogF(plat::LOG_ERROR, "Couldn't read from serial port.\n");
			CloseHandle(SerialHandle);
			return;
		}
		if (Buffer != '\0') { TextBuffer[TextCursor++] = Buffer; }
		
		if (TextCursor >= 256 || (Buffer == '\n' && TextCursor > 1))
		{
			if (!FirstRun) // Throw out first line because its usually trash
			{
				sensor_values Values = ParseSerialValues(TextBuffer, plat::GetTimeLocal());

				string BuiltString = SerializeSensorValues(Values);
				YieldLine(BuiltString, DataFile, DataQueue);
			}
			ZeroMemory(TextBuffer, 256);
			
			TextCursor = 0;
			FirstRun = false;
		}
	}
	CleanupSockets();
}

#if 0
void
TestDummyData(char* Hostname = null, char* Port = null)
{
	InitializeSockets();
	connection Server = { INVALID_SOCKET };
	bool32 SendResult;


	while (true){

		sensor_values Values;
		for (int i = 0 ; i < LOAD_CHANNELS + PRES_CHANNELS + TEMP_CHANNELS; ++i)
		{
			Values.Values[i] = ((rand() % 1024) / 1024.f) * 5.0f;
		}

		Sleep(50);
		string BuiltString = SerializeSensorValues(Values, false);
		YieldLine(BuiltString, DataFile);

		if (Server.Socket == INVALID_SOCKET)
		{
			LogF(plat::LOG_INFO, "Attempting to acquire remote connection...\n");
			if (Hostname == null) { Hostname = HOSTNAME; }
			if (Port == null) { Port = PORT; }
			Server = ConnectOverTCP(Hostname, Port);
		}
		else
		{
			SendResult = Send(Server, BuiltString.Value);
			if (!SendResult)
			{
				LogF(plat::LOG_INFO, "Lost connection to remote.\n");
				DisableSend(Server);
				CloseConnection(Server);
				Server.Socket = INVALID_SOCKET; // Will try to reacquire again next loop.
			}
		}

		FreeString(&BuiltString);
	}
	CleanupSockets();
}
#endif
